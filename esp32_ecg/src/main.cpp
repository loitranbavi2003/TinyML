#include <Arduino.h>
#include "tensorflow/lite/micro/all_ops_resolver.h"
#include "tensorflow/lite/micro/micro_error_reporter.h"
#include "tensorflow/lite/micro/micro_interpreter.h"
#include "tensorflow/lite/schema/schema_generated.h"
#include <tensorflow/lite/schema/schema_generated.h>
#include <tensorflow/lite/version.h>

#include "ecg_recognition6.h"

const tflite::Model* tflModel = nullptr;
tflite::MicroInterpreter* tflInterpreter = nullptr;
TfLiteTensor* tflInput1Tensor = nullptr;
TfLiteTensor* tflInput2Tensor = nullptr;
TfLiteTensor* tflOutputTensor = nullptr;
constexpr int tensorArenaSize = 32 * 1024;
alignas(16) byte tensorArena[tensorArenaSize];

/* index - value R Q S */ 
static int16_t arr_QRS[6] = {60,227,56,39,63,11};
static int16_t arr_data_ecg[187] = {228,239,147,61,13,35,17,18,6,14,4,8,0,4,0,1,0,2,1,8,11,19,27,35,48,52,63,70,76,79,82,81,78,72,69,63,58,52,53,51,51,52,56,61,64,72,71,61,51,47,47,46,47,47,45,46,39,48,105,205,227,145,61,11,30,37,30,31,30,25,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

void setup() 
{
  Serial.begin(115200);

  Serial.println("--- Map the model into a usable data structure ---");
  tflModel = tflite::GetModel(ecg_model_quant6_tflite);
  if (tflModel->version() != TFLITE_SCHEMA_VERSION) {
    Serial.println("Model schema mismatch!");
    while (1);
  }

  tflite::MicroErrorReporter tflErrorReporter;
  static tflite::AllOpsResolver resolver;
  tflInterpreter = new tflite::MicroInterpreter(tflModel, resolver, tensorArena, tensorArenaSize, &tflErrorReporter);
  tflInterpreter->AllocateTensors();
  tflInput1Tensor = tflInterpreter->input(0);
  tflInput2Tensor = tflInterpreter->input(1);
  tflOutputTensor = tflInterpreter->output(0);

  delay(1000);
}

void loop() 
{
  Serial.println("Recording...");

  for (int i = 0; i < 187; i++)
  {
    tflInput1Tensor->data.int8[i] = arr_data_ecg[i];
  }

  for (int i = 0; i < 6; i++)
  {
    tflInput2Tensor->data.int8[i] = arr_QRS[i];
  }

  long start = millis();
  // Run inference
  tflInterpreter->Invoke();

  int8_t* output = tflOutputTensor->data.int8;
  long stop = millis();

  Serial.print("Running time: ");
  Serial.println(stop - start);
  for (int i = 0; i < 5; i++)
  {
    Serial.print(output[i]);
    Serial.print(" ");
  }
  Serial.print("\n");

  delay(2000);
}